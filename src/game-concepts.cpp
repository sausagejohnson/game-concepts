#include "orx.h"

orxOBJECT *scene;
orxSTRING lastTrackName = "";

orxSTATUS orxFASTCALL Init()
{
    /* Creates the viewport */
    orxViewport_CreateFromConfig("Viewport");

    /* Creates the object */
	scene = orxObject_CreateFromConfig("Scene");

    /* Done! */
    return orxSTATUS_SUCCESS;
}


int GetTrackIndexFromConfig() {
	int index = 0;

	orxConfig_PushSection("Presentation");
	index = orxConfig_GetU32("Index");
	orxConfig_PopSection();

	return index;
}

void IncrementTrackIndexToConfig() {
	int index = GetTrackIndexFromConfig();
	int length = 1;

	orxConfig_PushSection("Presentation");
	length = orxConfig_GetListCounter("Tracks");

	index++;

	if (index >= length) {
		index = 0;
	}

	orxConfig_SetU32("Index", index);
	orxConfig_PopSection();

}

const orxSTRING GetNextTrackNameFromConfig() {
	int index = GetTrackIndexFromConfig();

	orxConfig_PushSection("Presentation");
	const orxCHAR *trackName = orxConfig_GetListString("Tracks", index);
	orxConfig_PopSection();

	IncrementTrackIndexToConfig();

	return trackName;
} 

orxSTATUS orxFASTCALL Run()
{
    orxSTATUS eResult = orxSTATUS_SUCCESS;

    /* Should quit? */
    if (orxInput_IsActive("Quit"))
    {
        /* Updates result */
        eResult = orxSTATUS_FAILURE;
    }

	if (orxInput_IsActive("Next") == orxTRUE && orxInput_HasNewStatus("Next") == orxTRUE)
	{
		const orxSTRING trackName = GetNextTrackNameFromConfig();
		
		//if (orxString_Compare(lastTrackName, "") == 0) {
		//	orxObject_RemoveTimeLineTrack(scene, lastTrackName);
		//}
		
		orxLOG("Track to add is: %s", trackName);


		orxObject_AddTimeLineTrack(scene, trackName);

		//lastTrackName = &trackName;
	}

    /* Done! */
    return eResult;
}

/** Exit function
 */
void orxFASTCALL Exit()
{
    /* Lets Orx clean all our mess automatically. :) */
}

/** Bootstrap function
 */
orxSTATUS orxFASTCALL Bootstrap()
{
    orxSTATUS eResult = orxSTATUS_SUCCESS;

    /* Adds a config storage to find the initial config file */
    orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);

    /* Done! */
    return eResult;
}

/** Main function
 */
int main(int argc, char **argv)
{
    /* Sets bootstrap function to provide at least one resource storage before loading any config files */
    orxConfig_SetBootstrap(Bootstrap);

    /* Executes a new instance of tutorial */
    orx_Execute(argc, argv, Init, Run, Exit);

    /* Done! */
    return EXIT_SUCCESS;
}

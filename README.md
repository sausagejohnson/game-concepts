# README #

Game Concepts real-time Presentaton for use in up-front talks and demonstrations on game development.

### What is this repository for? ###

* To provide a real-time demonstration of game related concepts. This is an application acting as a presentation.
* Objects, physics, FX, Spanwers and translations are demonstrated.

### How do I get set up? ###

* Clone repo.
* Ensure you have the git version of Orx cloned and built.
* Compile and run. Press Space to move between each demonstration.

### Who do I talk to? ###

* Get in touch on gitter.im/orx/orx or message here.